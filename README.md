# Tinklas

### Setup

First register the `social` navigation location using `register_nav_menus` in your `functions.php` file.

After that:

1. create a menu
2. add your social network links as custom links
3. assign the location of the menu

In your theme call `(new Tinklas)->display();`.

### How does it work

The plugin determines the social network using the URL of the custom link.

By default the plugin will pick up the icons from the `assets/svg/` folder and inserts the icon before the title. It also wraps the icon and the title in a `span` tag.

*If it does not find the proper icon it will skip that link.*

### Customization

You can pass the same arguments as for `wp_nav_menu` to the class.

#### Filters

- `tinklas_networks`
- `tinklas_network_icons`
- `tinklas_ico_path`
- `tinklas_ico_format`

#### Links

- https://codex.wordpress.org/Function_Reference/register_nav_menus
- https://codex.wordpress.org/Functions_File_Explained
- https://codex.wordpress.org/Appearance_Menus_Screen#Create_a_Menu
- https://codex.wordpress.org/Appearance_Menus_Screen#Custom_Links
