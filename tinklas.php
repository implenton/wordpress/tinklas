<?php
/*
 * Plugin Name:       Tinklas
 * Description:       Social network links as Nav menu
 * Plugin URI:        https://gitlab.com/implenton/wordpress/tinklas
 * Version:           1.2.3
 * Author:            implenton
 * Author URI:        https://implenton.com/
 * License:           GPLv3
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.txt
 * GitLab Plugin URI: implenton/wordpress/tinklas
 */

if ( ! defined( 'WPINC' ) ) {
    die;
}

class Tinklas {

    protected $networks = [
        'facebook',
        'twitter',
        'instagram',
        'linkedin',
        'google',
        'dribble',
        'youtube',
        'pinterest',
        'tumblr',
    ];

    protected $network_icons = [
        'facebook'  => 'facebook',
        'twitter'   => 'twitter',
        'instagram' => 'instagram',
        'linkedin'  => 'linkedin',
        'google'    => 'google-plus',
        'dribble'   => 'dribble',
        'youtube'   => 'youtube',
        'pinterest' => 'pinterest',
        'tumblr'    => 'tumblr',
    ];

    protected $nav_args = [
        'container'      => '',
        'theme_location' => 'social',
        'menu_class'     => 'menu',
    ];

    /**
     * Tinklas constructor.
     *
     * @param array $args Optional. Array of nav menu arguments.
     */
    function __construct( $args = [] ) {

        $this->nav_args      = array_merge( $this->nav_args, $args );
        $this->networks      = apply_filters( 'tinklas_networks', $this->networks );
        $this->network_icons = apply_filters( 'tinklas_network_icons', $this->network_icons );

        add_filter( 'nav_menu_item_title', [ $this, 'prepend_ico' ], 10, 4 );

    }

    /**
     * Displays a navigation menu.
     *
     * @return string|void
     */
    public function display() {

        if ( has_nav_menu( $this->nav_args['theme_location'] ) ) {

            wp_nav_menu( $this->nav_args );

        }

    }

    /**
     * Attached filter that would insert the icon if eligible.
     *
     * @param $title
     * @param $item
     * @param $args
     * @param $depth
     *
     * @return string
     */
    public function prepend_ico( $title, $item, $args, $depth ) {

        if ( ! $this->check_location( $args->theme_location ) ) {

            return $title;

        }

        if ( ! $social = $this->get_social_network( $item->url ) ) {

            return $title;

        }

        if ( ! $ico = $this->get_ico( $social ) ) {

            return $title;

        }

        return $this->prepare_title( $social, $ico, $title, $this->nav_args['menu_class'] );

    }

    /**
     * Checks if we should prepend the icon to the current element.
     *
     * @param $location
     *
     * @return bool
     */
    protected function check_location( $location ) {

        return $location == $this->nav_args['theme_location'];

    }

    /**
     * Determines the social network name based on the url.
     *
     * @param $url
     *
     * @return bool|string
     */
    protected function get_social_network( $url ) {

        foreach ( $this->networks as $network ) {

            if ( strpos( $url, $network ) !== false ) {

                return $network;

            }

        }

        return false;

    }

    /**
     * Retreives the icon from a folder.
     *
     * @param $network
     *
     * @return bool|string
     */
    protected function get_ico( $network ) {

        $path   = apply_filters( 'tinklas_ico_path', 'assets/svg/' );
        $format = apply_filters( 'tinklas_ico_format', 'svg' );
        $file   = $this->network_icons[ $network ];

        $ico = get_theme_file_path( $path . $file . '.' . $format );

        if ( ! file_exists( $ico ) ) {

            return false;

        }

        return file_get_contents( $ico );

    }

    /**
     * Returns the title with the prepended icon.
     *
     * @param $social
     * @param $ico
     * @param $title
     * @param $menu_class
     *
     * @return string
     */
    protected function prepare_title( $social, $ico, $title, $menu_class ) {

        return sprintf( '<span class="%4$s-item__ico %4$s-item__ico--%1$s">%2$s</span><span class="%4$s-item__title %4$s-item__title--%1$s">%3$s</span>',
            $social, $ico, $title, $menu_class );

    }
}